///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __FONTSETTINGSDIALOG_H__
#define __FONTSETTINGSDIALOG_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/panel.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/spinctrl.h>
#include <wx/statbox.h>
#include <wx/combobox.h>
#include <wx/dialog.h>
#include <wx/colordlg.h>

#include "Panel_PixelatedTextPanel.h"
///////////////////////////////////////////////////////////////////////////
#define wxID_BUTTON_BKG_COLOR 1000
#define wxID_BUTTON_PIX_COLOR 1001
#define wxID_BUTTON_GRID_COLOR 1002
#define wxID_BUTTON_BRD_COLOR 1003
#define wxID_COMBO_PIX_SIZE 1004
#define wxID_COMBO_FONT_MODE 1005
#define wxID_COMBO_BYTE_MODE 1006
#define wxID_PANEL_PERVIEW 1007

///////////////////////////////////////////////////////////////////////////////
/// Class Dialog_Settings
///////////////////////////////////////////////////////////////////////////////
class Dialog_Settings : public wxDialog
{
	private:
        wxPanel* m_pclsPanel_BackgroundColor;
		wxButton* m_pclsButton_BackgroundColorSetting;
		wxPanel* m_pclsPanel_Pixel;
		wxButton* m_pclsButton_PixelColorSetting;
		wxPanel* m_pclsPanel_Grid;
		wxButton* m_pclsButton_GridColorSetting;
		wxPanel* m_pclsPanel_FocusBorder;
		wxButton* m_pclsButton_GridColorFocusBorder;
		wxSpinCtrl* m_pclsSpinCtrl_PixelSize;
		wxComboBox* m_pclsComboBox_FontMode;
		wxStaticText* pclsStaticText_DataMode;
		wxComboBox* m_pclsComboBox_DataMode;
		Panel_PixelatedTextPanel* m_pclsPanel_PerviewPanel;
		wxComboBox* m_pclsComboBox_BytesEachRow;
		wxComboBox* m_pclsComboBox_ByteHeader;
		wxComboBox* m_pclsComboBox_ByteTail;
		wxStdDialogButtonSizer* pclsStdDialogButtonSizer_DialogButtons;
		wxButton* pclsStdDialogButtonSizer_DialogButtonsOK;
		wxButton* pclsStdDialogButtonSizer_DialogButtonsCancel;

		void wxEvent_OnDialogInitialize(wxInitDialogEvent& event)		{UpdatePreview();}
		void wxEvent_OnShow(wxShowEvent& event)							{UpdatePreview();}
		void wxEvent_OnPerviewUpdate(wxUpdateUIEvent& event)			{UpdatePreview();}
		void wxEvent_OnSetBackgroundColor(wxCommandEvent& event);
		void wxEvent_OnSetPixelColor(wxCommandEvent& event);
		void wxEvent_OnSetGridColor(wxCommandEvent& event);
		void wxEvent_OnSetFocusBorderColor(wxCommandEvent& event);
	protected:
		virtual void UpdatePreview(void);
		virtual int SelectColor(wxColor& clsColor, const wxString& strDialogTitle = wxEmptyString);
		virtual void OnSetColor(wxCommandEvent& event, wxPanel* pclsPerviewPanel);
	public:

		Dialog_Settings( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Settings"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );
		~Dialog_Settings();

		DECLARE_EVENT_TABLE();
};

#endif //__FONTSETTINGSDIALOG_H__

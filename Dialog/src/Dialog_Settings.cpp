///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////
#include "Dialog_Settings.h"

#define CONST_PERVIEW_SIZE_WIDTH			(24)
#define CONST_PERVIEW_SIZE_HEIGHT			(24)
#define CONST_PERVIEW_PIXEL_SIZE			(4)
#define CONST_PERVIEW_EDGE_WIDTH			(1)
#define CONST_PERVIEW_PANEL_SIZE_WIDTH		(CONST_PERVIEW_SIZE_WIDTH*CONST_PERVIEW_PIXEL_SIZE+CONST_PERVIEW_EDGE_WIDTH*2+1)
#define CONST_PERVIEW_PANEL_SIZE_HEIGHT		(CONST_PERVIEW_SIZE_HEIGHT*CONST_PERVIEW_PIXEL_SIZE+CONST_PERVIEW_EDGE_WIDTH*2+1)
///////////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(Dialog_Settings,wxDialog)
    EVT_BUTTON      (wxID_BUTTON_BKG_COLOR, Dialog_Settings::wxEvent_OnSetBackgroundColor)
	EVT_BUTTON      (wxID_BUTTON_PIX_COLOR, Dialog_Settings::wxEvent_OnSetPixelColor)
	EVT_BUTTON      (wxID_BUTTON_GRID_COLOR, Dialog_Settings::wxEvent_OnSetGridColor)
	EVT_BUTTON      (wxID_BUTTON_BRD_COLOR, Dialog_Settings::wxEvent_OnSetFocusBorderColor)
	EVT_UPDATE_UI	(wxID_PANEL_PERVIEW, Dialog_Settings::wxEvent_OnPerviewUpdate)
END_EVENT_TABLE()
Dialog_Settings::Dialog_Settings( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) :
wxDialog( parent, id, title, pos, size, style )
{
    this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxFlexGridSizer* pclsFlexGridSizer_Root;
	pclsFlexGridSizer_Root = new wxFlexGridSizer( 3, 1, 0, 0 );
	pclsFlexGridSizer_Root->SetFlexibleDirection( wxBOTH );
	pclsFlexGridSizer_Root->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxBoxSizer* pclsSizer_Row0;
	pclsSizer_Row0 = new wxBoxSizer( wxHORIZONTAL );

	wxStaticBoxSizer* pclsStaticBoxSizer_PixelPanelSettings;
	pclsStaticBoxSizer_PixelPanelSettings = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Pixel Panel Settings") ), wxVERTICAL );

	wxFlexGridSizer* pclsFlexGridSizer_ColorSettings;
	pclsFlexGridSizer_ColorSettings = new wxFlexGridSizer( 2, 4, 0, 0 );
	pclsFlexGridSizer_ColorSettings->SetFlexibleDirection( wxBOTH );
	pclsFlexGridSizer_ColorSettings->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_pclsPanel_BackgroundColor = new wxPanel( pclsStaticBoxSizer_PixelPanelSettings->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxSize( 30,30 ), wxTAB_TRAVERSAL );
	m_pclsPanel_BackgroundColor->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	pclsFlexGridSizer_ColorSettings->Add( m_pclsPanel_BackgroundColor, 1, wxEXPAND | wxALL, 5 );

	m_pclsButton_BackgroundColorSetting = new wxButton( pclsStaticBoxSizer_PixelPanelSettings->GetStaticBox(), wxID_BUTTON_BKG_COLOR, wxT("Background Color"), wxDefaultPosition, wxSize( 125,30 ), 0 );
	pclsFlexGridSizer_ColorSettings->Add( m_pclsButton_BackgroundColorSetting, 0, wxALIGN_CENTER_VERTICAL, 5 );

	m_pclsPanel_Pixel = new wxPanel( pclsStaticBoxSizer_PixelPanelSettings->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxSize( 30,30 ), wxTAB_TRAVERSAL );
	m_pclsPanel_Pixel->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	pclsFlexGridSizer_ColorSettings->Add( m_pclsPanel_Pixel, 1, wxEXPAND | wxALL, 5 );

	m_pclsButton_PixelColorSetting = new wxButton( pclsStaticBoxSizer_PixelPanelSettings->GetStaticBox(), wxID_BUTTON_PIX_COLOR, wxT("Pixel Color"), wxDefaultPosition, wxSize( 125,30 ), 0 );
	pclsFlexGridSizer_ColorSettings->Add( m_pclsButton_PixelColorSetting, 0, wxALIGN_CENTER_VERTICAL, 5 );

	m_pclsPanel_Grid = new wxPanel( pclsStaticBoxSizer_PixelPanelSettings->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxSize( 30,30 ), wxTAB_TRAVERSAL );
	m_pclsPanel_Grid->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	pclsFlexGridSizer_ColorSettings->Add( m_pclsPanel_Grid, 1, wxEXPAND | wxALL, 5 );

	m_pclsButton_GridColorSetting = new wxButton( pclsStaticBoxSizer_PixelPanelSettings->GetStaticBox(), wxID_BUTTON_GRID_COLOR, wxT("Grid Color"), wxDefaultPosition, wxSize( 125,30 ), 0 );
	pclsFlexGridSizer_ColorSettings->Add( m_pclsButton_GridColorSetting, 0, wxALIGN_CENTER_VERTICAL, 5 );

	m_pclsPanel_FocusBorder = new wxPanel( pclsStaticBoxSizer_PixelPanelSettings->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxSize( 30,30 ), wxTAB_TRAVERSAL );
	m_pclsPanel_FocusBorder->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	pclsFlexGridSizer_ColorSettings->Add( m_pclsPanel_FocusBorder, 1, wxEXPAND | wxALL, 5 );

	m_pclsButton_GridColorFocusBorder = new wxButton( pclsStaticBoxSizer_PixelPanelSettings->GetStaticBox(), wxID_BUTTON_BRD_COLOR, wxT("Focus Border Color"), wxDefaultPosition, wxSize( 125,30 ), 0 );
	pclsFlexGridSizer_ColorSettings->Add( m_pclsButton_GridColorFocusBorder, 0, wxALIGN_CENTER_VERTICAL, 5 );


	pclsStaticBoxSizer_PixelPanelSettings->Add( pclsFlexGridSizer_ColorSettings, 0, wxEXPAND, 5 );

	wxFlexGridSizer* pclsFlexGridSizer_Size;
	pclsFlexGridSizer_Size = new wxFlexGridSizer( 1, 2, 0, 0 );
	pclsFlexGridSizer_Size->SetFlexibleDirection( wxBOTH );
	pclsFlexGridSizer_Size->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxStaticText* pclsStaticText_PixelSize;
	pclsStaticText_PixelSize = new wxStaticText( pclsStaticBoxSizer_PixelPanelSettings->GetStaticBox(), wxID_ANY, wxT("Panel Pixel Size:"), wxDefaultPosition, wxDefaultSize, 0 );
	pclsStaticText_PixelSize->Wrap( -1 );
	pclsFlexGridSizer_Size->Add( pclsStaticText_PixelSize, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_pclsSpinCtrl_PixelSize = new wxSpinCtrl( pclsStaticBoxSizer_PixelPanelSettings->GetStaticBox(), wxID_COMBO_PIX_SIZE, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 3, 5, 4 );
	pclsFlexGridSizer_Size->Add( m_pclsSpinCtrl_PixelSize, 1, wxALIGN_CENTER_VERTICAL, 5 );


	pclsStaticBoxSizer_PixelPanelSettings->Add( pclsFlexGridSizer_Size, 1, wxEXPAND, 5 );


	pclsSizer_Row0->Add( pclsStaticBoxSizer_PixelPanelSettings, 0, wxEXPAND, 5 );

	wxStaticBoxSizer* pclsStaticBoxSizer_FontDataMode;
	pclsStaticBoxSizer_FontDataMode = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Font Data Mode") ), wxHORIZONTAL );

	wxFlexGridSizer* pclsFlexGridSizer_FontDataModeSettings;
	pclsFlexGridSizer_FontDataModeSettings = new wxFlexGridSizer( 2, 2, 0, 0 );
	pclsFlexGridSizer_FontDataModeSettings->SetFlexibleDirection( wxBOTH );
	pclsFlexGridSizer_FontDataModeSettings->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxStaticText* pclsStaticText_FontMode;
	pclsStaticText_FontMode = new wxStaticText( pclsStaticBoxSizer_FontDataMode->GetStaticBox(), wxID_ANY, wxT("Font Mode:"), wxDefaultPosition, wxDefaultSize, 0 );
	pclsStaticText_FontMode->Wrap( -1 );
	pclsFlexGridSizer_FontDataModeSettings->Add( pclsStaticText_FontMode, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_pclsComboBox_FontMode = new wxComboBox( pclsStaticBoxSizer_FontDataMode->GetStaticBox(), wxID_COMBO_FONT_MODE, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
	m_pclsComboBox_FontMode->Append( wxT("Mode 1") );
	m_pclsComboBox_FontMode->Append( wxT("Mode 2") );
	m_pclsComboBox_FontMode->Append( wxT("Mode 3") );
	m_pclsComboBox_FontMode->Append( wxT("Mode 4") );
	m_pclsComboBox_FontMode->SetSelection( 0 );
	pclsFlexGridSizer_FontDataModeSettings->Add( m_pclsComboBox_FontMode, 1, wxALL|wxEXPAND, 5 );

	pclsStaticText_DataMode = new wxStaticText( pclsStaticBoxSizer_FontDataMode->GetStaticBox(), wxID_ANY, wxT("Data Mode:"), wxDefaultPosition, wxDefaultSize, 0 );
	pclsStaticText_DataMode->Wrap( -1 );
	pclsFlexGridSizer_FontDataModeSettings->Add( pclsStaticText_DataMode, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_pclsComboBox_DataMode = new wxComboBox( pclsStaticBoxSizer_FontDataMode->GetStaticBox(), wxID_COMBO_BYTE_MODE, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
	m_pclsComboBox_DataMode->Append( wxT("Low bit starting") );
	m_pclsComboBox_DataMode->Append( wxT("High bit starting") );
	m_pclsComboBox_DataMode->SetSelection( 0 );
	pclsFlexGridSizer_FontDataModeSettings->Add( m_pclsComboBox_DataMode, 1, wxALL|wxEXPAND, 5 );


	pclsStaticBoxSizer_FontDataMode->Add( pclsFlexGridSizer_FontDataModeSettings, 1, wxEXPAND, 5 );

	wxBoxSizer* pclsSizer_FontDataModePreview;
	pclsSizer_FontDataModePreview = new wxBoxSizer( wxVERTICAL );

	m_pclsPanel_PerviewPanel = new Panel_PixelatedTextPanel( pclsStaticBoxSizer_FontDataMode->GetStaticBox(), wxID_PANEL_PERVIEW, wxDefaultPosition);
	m_pclsPanel_PerviewPanel->SetPixelNumber(CONST_PERVIEW_SIZE_WIDTH, CONST_PERVIEW_SIZE_HEIGHT);
	m_pclsPanel_PerviewPanel->SetEdgeWidth(CONST_PERVIEW_EDGE_WIDTH);
	m_pclsPanel_PerviewPanel->SetPixelSize(CONST_PERVIEW_PIXEL_SIZE);
	m_pclsPanel_PerviewPanel->CleanPanel();
	m_pclsPanel_PerviewPanel->SetPixelUnitColor(23, 23, wxColor(255, 255, 255));
	pclsSizer_FontDataModePreview->Add( m_pclsPanel_PerviewPanel, 1, wxEXPAND | wxALL, 5 );


	pclsStaticBoxSizer_FontDataMode->Add( pclsSizer_FontDataModePreview, 0, wxEXPAND, 5 );


	pclsSizer_Row0->Add( pclsStaticBoxSizer_FontDataMode, 0, wxEXPAND, 5 );


	pclsFlexGridSizer_Root->Add( pclsSizer_Row0, 1, wxEXPAND, 5 );

	wxBoxSizer* pclsSizer_Row1;
	pclsSizer_Row1 = new wxBoxSizer( wxVERTICAL );

	wxStaticBoxSizer* pclsStaticBoxSizer_FontDataFormat;
	pclsStaticBoxSizer_FontDataFormat = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Data Foramt") ), wxVERTICAL );

	wxFlexGridSizer* pclsFlexGridSizer_DataFormatSettings;
	pclsFlexGridSizer_DataFormatSettings = new wxFlexGridSizer( 0, 6, 0, 0 );
	pclsFlexGridSizer_DataFormatSettings->SetFlexibleDirection( wxBOTH );
	pclsFlexGridSizer_DataFormatSettings->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxStaticText* pclsStaticText_BytesEachRow;
	pclsStaticText_BytesEachRow = new wxStaticText( pclsStaticBoxSizer_FontDataFormat->GetStaticBox(), wxID_ANY, wxT("Bytes Each Row:"), wxDefaultPosition, wxDefaultSize, 0 );
	pclsStaticText_BytesEachRow->Wrap( -1 );
	pclsFlexGridSizer_DataFormatSettings->Add( pclsStaticText_BytesEachRow, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_pclsComboBox_BytesEachRow = new wxComboBox( pclsStaticBoxSizer_FontDataFormat->GetStaticBox(), wxID_ANY, wxT("Auto"), wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	m_pclsComboBox_BytesEachRow->Append( wxT("<None>") );
	m_pclsComboBox_BytesEachRow->Append( wxT("Auto") );
	m_pclsComboBox_BytesEachRow->Append( wxT("6") );
	m_pclsComboBox_BytesEachRow->Append( wxT("8") );
	m_pclsComboBox_BytesEachRow->Append( wxT("12") );
	m_pclsComboBox_BytesEachRow->Append( wxT("16") );
	m_pclsComboBox_BytesEachRow->Append( wxT("24") );
	m_pclsComboBox_BytesEachRow->Append( wxT("32") );
	m_pclsComboBox_BytesEachRow->Append( wxT("64") );
	pclsFlexGridSizer_DataFormatSettings->Add( m_pclsComboBox_BytesEachRow, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxStaticText* pclsStaticText_ByteHeader;
	pclsStaticText_ByteHeader = new wxStaticText( pclsStaticBoxSizer_FontDataFormat->GetStaticBox(), wxID_ANY, wxT("Byte Header"), wxDefaultPosition, wxDefaultSize, 0 );
	pclsStaticText_ByteHeader->Wrap( -1 );
	pclsFlexGridSizer_DataFormatSettings->Add( pclsStaticText_ByteHeader, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_pclsComboBox_ByteHeader = new wxComboBox( pclsStaticBoxSizer_FontDataFormat->GetStaticBox(), wxID_ANY, wxT("0x"), wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	m_pclsComboBox_ByteHeader->Append( wxT("<None>") );
	m_pclsComboBox_ByteHeader->Append( wxT("0") );
	m_pclsComboBox_ByteHeader->Append( wxT("0x") );
	pclsFlexGridSizer_DataFormatSettings->Add( m_pclsComboBox_ByteHeader, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxStaticText* m_pclsStaticText_ByteTail;
	m_pclsStaticText_ByteTail = new wxStaticText( pclsStaticBoxSizer_FontDataFormat->GetStaticBox(), wxID_ANY, wxT("Byte Tail:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pclsStaticText_ByteTail->Wrap( -1 );
	pclsFlexGridSizer_DataFormatSettings->Add( m_pclsStaticText_ByteTail, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_pclsComboBox_ByteTail = new wxComboBox( pclsStaticBoxSizer_FontDataFormat->GetStaticBox(), wxID_ANY, wxT("<None>"), wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	m_pclsComboBox_ByteTail->Append( wxT("<None>") );
	m_pclsComboBox_ByteTail->Append( wxT("H") );
	m_pclsComboBox_ByteTail->Append( wxEmptyString );
	pclsFlexGridSizer_DataFormatSettings->Add( m_pclsComboBox_ByteTail, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	pclsStaticBoxSizer_FontDataFormat->Add( pclsFlexGridSizer_DataFormatSettings, 1, wxEXPAND, 5 );


	pclsSizer_Row1->Add( pclsStaticBoxSizer_FontDataFormat, 1, wxEXPAND, 5 );


	pclsFlexGridSizer_Root->Add( pclsSizer_Row1, 1, wxEXPAND, 5 );

	pclsStdDialogButtonSizer_DialogButtons = new wxStdDialogButtonSizer();
	pclsStdDialogButtonSizer_DialogButtonsOK = new wxButton( this, wxID_OK );
	pclsStdDialogButtonSizer_DialogButtons->AddButton( pclsStdDialogButtonSizer_DialogButtonsOK );
	pclsStdDialogButtonSizer_DialogButtonsCancel = new wxButton( this, wxID_CANCEL );
	pclsStdDialogButtonSizer_DialogButtons->AddButton( pclsStdDialogButtonSizer_DialogButtonsCancel );
	pclsStdDialogButtonSizer_DialogButtons->Realize();

	pclsFlexGridSizer_Root->Add( pclsStdDialogButtonSizer_DialogButtons, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( pclsFlexGridSizer_Root );
	this->Layout();
	pclsFlexGridSizer_Root->Fit( this );

	this->Centre( wxBOTH );
}

void Dialog_Settings::UpdatePreview(void)
{
	m_pclsPanel_PerviewPanel->RefreshDisplay();
}

void Dialog_Settings::OnSetColor(wxCommandEvent& event, wxPanel* pclsPerviewPanel)
{
    wxColor             clsColor;
    int                 iDlgRet;

    if(NULL != pclsPerviewPanel)
    {
        clsColor = pclsPerviewPanel->GetBackgroundColour();
    }
    iDlgRet = SelectColor(clsColor, wxT("Select a color."));
    if(wxID_OK == iDlgRet)
    {
        if(NULL != pclsPerviewPanel)
        {
            pclsPerviewPanel->SetBackgroundColour(clsColor);
            pclsPerviewPanel->Refresh();
        }
    }
}

void Dialog_Settings::wxEvent_OnSetBackgroundColor(wxCommandEvent& event)
{
    OnSetColor(event, m_pclsPanel_BackgroundColor);
    m_pclsPanel_PerviewPanel->SetBackgroundColor(m_pclsPanel_BackgroundColor->GetBackgroundColour());
    m_pclsPanel_PerviewPanel->RefreshDisplay();
}

void Dialog_Settings::wxEvent_OnSetPixelColor(wxCommandEvent& event)
{
    OnSetColor(event, m_pclsPanel_Pixel);
    m_pclsPanel_PerviewPanel->SetPixelColor(m_pclsPanel_Pixel->GetBackgroundColour());
    m_pclsPanel_PerviewPanel->RefreshDisplay();
}

void Dialog_Settings::wxEvent_OnSetGridColor(wxCommandEvent& event)
{
    OnSetColor(event, m_pclsPanel_Grid);
    m_pclsPanel_PerviewPanel->SetGridColor(m_pclsPanel_Grid->GetBackgroundColour());
    m_pclsPanel_PerviewPanel->RefreshDisplay();
}

void Dialog_Settings::wxEvent_OnSetFocusBorderColor(wxCommandEvent& event)
{
    OnSetColor(event, m_pclsPanel_FocusBorder);
    m_pclsPanel_PerviewPanel->SetEdgeColor(m_pclsPanel_FocusBorder->GetBackgroundColour());
    m_pclsPanel_PerviewPanel->RefreshDisplay();
}

int Dialog_Settings::SelectColor(wxColor& clsColor, const wxString& strDialogTitle)
{
    wxColourDialog*     pclsColourDialog;
    wxColourData*       pclsColourData;
    int                 iDlgRet;

    pclsColourData = new wxColourData();
    pclsColourDialog = new wxColourDialog(this, pclsColourData);

    iDlgRet = wxID_CANCEL;
    if((NULL != pclsColourData) &&(NULL != pclsColourDialog))
    {
        pclsColourDialog->SetTitle(strDialogTitle);
        iDlgRet = pclsColourDialog->ShowModal();
        if(wxID_OK == iDlgRet)
        {
            *pclsColourData = pclsColourDialog->GetColourData();
            clsColor = pclsColourData->GetColour();
        }
    }
    delete pclsColourData;
    delete pclsColourDialog;

    return iDlgRet;
}

Dialog_Settings::~Dialog_Settings()
{
}

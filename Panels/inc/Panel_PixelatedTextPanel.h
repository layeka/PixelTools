///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDE_CLASS_PANEL_PAINT_H__
#define __INCLUDE_CLASS_PANEL_PAINT_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/panel.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/list.h>
#include <wx/listimpl.cpp>
#include <wx/dcclient.h>
#include "wxPixelatedPanel.h"

///////////////////////////////////////////////////////////////////////////
WX_DECLARE_LIST(wxBitmap, FontPixelMatrixList);
///////////////////////////////////////////////////////////////////////////////
/// Class wxFontPixelMatrixPanel
///////////////////////////////////////////////////////////////////////////////
class wxFontPixelMatrixCoordinate
{
	private:
		int32_t					m_iIndex;
		int32_t					m_iPosition;
		int32_t					m_iWidth;
		int32_t					m_iHeight;
	public:
								wxFontPixelMatrixCoordinate(int32_t iIndex=-1, int32_t iPosition=-1, int32_t iWidth=-1, int32_t iHeight = -1)
																												{SetParameter(iIndex, iPosition, iWidth, iHeight);}
								~wxFontPixelMatrixCoordinate(void)												{}
		void					SetIndex(int32_t iIndex)														{m_iIndex = iIndex;}
		void					SetPosition(int32_t iPosition)													{m_iPosition = iPosition;}
		void					SetWidth(int32_t iWidth)														{m_iWidth = iWidth;}
		void					SetHeight(int32_t iHeight)														{m_iHeight = iHeight;}
		void					SetParameter(int32_t iIndex, int32_t iPosition, int32_t iWidth, int32_t iHeight){m_iIndex = iIndex; m_iPosition = iPosition; m_iWidth = iWidth;m_iHeight = iHeight;}
		int32_t					GetIndex(void)																	{return m_iIndex;}
		int32_t					GetPosition(void)																{return m_iPosition;}
		int32_t					GetWidth(void)																	{return m_iWidth;}
		int32_t					GetHeight(void)																	{return m_iHeight;}
		void					CopyForm(wxFontPixelMatrixCoordinate& clsSrc)									{m_iIndex = clsSrc.GetIndex(); m_iPosition = clsSrc.GetPosition(); m_iWidth = clsSrc.GetWidth();}
		void					Reset(void)																		{m_iIndex = -1; m_iPosition = -1; m_iWidth = -1;}
};

class Panel_PixelatedTextPanel : public wxPixelatedPanel
{
	private:

		FontPixelMatrixList*	m_pclsFontPixelMatrixList;
		wxClientDC*				m_pclsCDC;
		wxBrush*				m_pclsBrush;
		wxPen*					m_pclsPen;
		wxFontPixelMatrixCoordinate* m_pclsCurrentFocus;
		wxColor*				m_pclsPixelColor;                               // Drawing pixel color.
		wxColor*				m_pclsBackgroundColor;                          // NO drawing pixel color.
		wxColor*				m_pclsFocusBorderColor;                         // Focused character hight light border color.
		wxFont*                 m_pclsTextFont;

		uint32_t				_getPosition(int32_t iCharacterGraphIndex = -1);
		void					_addFontPixelMatrix(wxBitmap& clsBitmap);
		void					_getFontGraphCoordinate(int32_t uiMousePosX, wxFontPixelMatrixCoordinate& clsResult);
		void					_highLightCharacter(wxFontPixelMatrixCoordinate& clsCoordinate, bool bHighLight = true);

		void					wxEvent_OnMouseEvent(wxMouseEvent& event)										{OnMouseEvent(event); event.Skip();}
		void					wxEvent_OnPaint(wxPaintEvent &event)											{OnPaint(event);event.Skip();}
	protected:
		virtual void			OnMouseEvent(wxMouseEvent& event);
		virtual	void			OnPaint(wxPaintEvent& event);
	public:
		void					AppendCharacter(wxBitmap& clsBitmap);
								Panel_PixelatedTextPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition);
								~Panel_PixelatedTextPanel();
		void					CleanPanel(void);
		void					SetBackgroundColor(const wxColor& clsColor);
		void					SetPixelColor(const wxColor& clsColor);
		void					SetFocusBorderColor(const wxColor& clsColor);
		wxColor&				GetBackgroundColor(void);
		wxColor&				GetPixelColor(void);
		wxColor&				GetBorderColor(void);
		void                    UpdateBackgroundColor(void);
		void                    UpdateDrawPixelColor(void);
		void                    SetFont(int iFontSize, wxFontFamily eFamily, wxFontStyle eStyle, wxFontWeight eWeight, bool bUnderlined, const wxString& strFace, wxFontEncoding eTextEncoding);
		wxFont&                 GetFont(void);

		DECLARE_EVENT_TABLE();
};

#endif //__INCLUDE_CLASS_PANEL_PAINT_H__

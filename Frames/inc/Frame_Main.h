///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __MATRIXTOOL_FRAME_H__
#define __MATRIXTOOL_FRAME_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/combobox.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/spinctrl.h>
#include <wx/toolbar.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/scrolwin.h>
#include <wx/splitter.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/fontenum.h>
#include "Panel_PixelatedTextPanel.h"

///////////////////////////////////////////////////////////////////////////

#define wxID_FRAME_MAIN 1000
#define wxID_COMBO_FONT_FACE 1001
#define wxID_CTRL_SPIN_FONT_SIZE 1002
#define wxID_TOOL_FONT_BOLD 1003
#define wxID_TOOL_FONT_ITALIC 1004
#define wxID_TOOL_FONT_TOP_CUT 1005
#define wxID_CTRL_SPIN_TOP_CUT 1006
#define wxID_TOOL_FONT_BOTTOM_CUT 1007
#define wxID_CTRL_SPIN_BOTTOM_CUT 1008
#define wxID_TOOL_FONT_FIXED_WIDTH 1009
#define wxID_CTRL_SPIN_FIXED_WIDTH 1010
#define wxID_TOOL_FONT_SETTINGS 1011
#define wxID_CTRL_TEXT_INPUT 1012

///////////////////////////////////////////////////////////////////////////////
/// Class Frame_Main
///////////////////////////////////////////////////////////////////////////////
class Frame_Main : public wxFrame
{
	private:
		wxToolBar* m_pclsToolBar_FontSet;
		wxComboBox* m_pclsComboBox_FontList;
		wxSpinCtrl* m_pclsSpinCtrl_FontSize;
		wxToolBarToolBase* m_pclsTool_Bold;
		wxToolBarToolBase* m_pclsTool_Italic;
		wxToolBarToolBase* m_pclsTool_TopCut;
		wxSpinCtrl* m_pclsSpinCtrl_TopCutValue;
		wxToolBarToolBase* m_pclsTool_BottomCut;
		wxSpinCtrl* m_pclsSpinCtrl_BottomCutValue;
		wxToolBarToolBase* m_pclsTool_ForceFixedWidth;
		wxSpinCtrl* m_pclsSpinCtrl_FixedWidthValue;
		wxToolBarToolBase* m_pclsTool_Settings;
		wxPanel* m_pclsPanel_RootPanel;
		wxTextCtrl* m_pclsTextCtrl_Input;

		wxSplitterWindow* m_pclsSplitter_OutPut;
		wxScrolledWindow* m_pclsScrolledWindow_Paint;

		Panel_PixelatedTextPanel* m_pclsPanel_PaintPanel;
		wxPanel* m_pclsPanel_OutputText;

		wxTextCtrl* m_pclsTextCtrl_Output;
		wxStatusBar* m_pclsStatusBar_Main;

		void				_initialize(void);
		void				_paintFont(void);
		void				_resizePaintPanel(void);
		void				_enumFonts(void);
		void                _createToolbar(void);

		void				wxEvent_OnPaint(wxPaintEvent &event)				{OnPaint(event);event.Skip();}
		void				wxEvent_OnRefreshPaint(wxCommandEvent &event)		{OnRefreshPaint();}
		void				wxEvent_OnTextEvent(wxCommandEvent &event)			{OnTextEvent(event);}
		void				wxEvent_OnFontSizeSelected(wxCommandEvent &event)	{OnFontSizeSelected(event);}
		void				wxEvent_OnFontFaceSelected(wxCommandEvent &event)	{OnFontFaceSelected(event);}
		void				wxEvent_OnSettings(wxCommandEvent &event)			{OnSettings(event);}
	protected:
		virtual void		OnPaint(wxPaintEvent &event);
		virtual	void		OnRefreshPaint(void);
		virtual	void		OnTextEvent(wxCommandEvent &event);
		virtual void		OnFontSizeSelected(wxCommandEvent &event);
		virtual void		OnFontFaceSelected(wxCommandEvent &event);
		virtual void		OnSettings(wxCommandEvent &event);
	public:

		Frame_Main( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("MatrixTool"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 800,480 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~Frame_Main();
		void				AppendFontFace(wxString& strFontFace);
		virtual	bool		Show(bool show = true);

		DECLARE_EVENT_TABLE();
};

#endif //__MATRIXTOOL_FRAME_H__

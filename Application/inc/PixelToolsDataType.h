#ifndef __INCLUDE_PIXELMATRIXFONTDATATYPE_H__
#define __INCLUDE_PIXELMATRIXFONTDATATYPE_H__

#define		DEFAULT_VALUE_DEFAULT_PIXEL_SIZE				(4)
#define		DEFAULT_VALUE_PANEL_BACKGROUND_COLOR			(0xFF000000)
#define		DEFAULT_VALUE_PANEL_PIXEL_COLOR					(0xFFFFFFFF)
#define		DEFAULT_VALUE_PANEL_GRID_COLOR					(0xFF808080)
#define		DEFAULT_VALUE_PANEL_FOCUS_BORDER_COLOR			(0xFFF0F0F0)

#define		PARAMETER_FORMAT_BYTE_HEAD_LENGTH_MAX			(8)

#define		SETTINGS_FILE_NAME								("Settings.bin")
#define		SETTINGS_FILE_PATH								(".\\")

#define		MULTI_LANGUAGE_TEXT(TEXT)						_(TEXT)

typedef union
{
	uint8_t					Channel[4];
	uint32_t				RGBA;
}RGBA_COLOR;

typedef enum
{
	MODE_1					= 0,
	MODE_2,
	MODE_3,
	MODE_4,
	MODE_MAX,
}FONT_MODULE_MODE;

typedef enum
{
	LOW_BIT_STARTING		= 0,
	HIGH_BIT_STARTING,
}FONT_BYTE_MODE;

typedef struct
{
	int32_t					SelectedFont;
	int32_t					Size;
    bool            		Bold;
    bool            		Italic;
}FONT_MODULE_OPTION;

typedef struct
{
    RGBA_COLOR              BackGround;
    RGBA_COLOR              Pixel;
    RGBA_COLOR              Grid;
    RGBA_COLOR              FocusBorder;
}FONT_PANEL_COLOR;

typedef struct
{
    FONT_PANEL_COLOR		Colors;
    uint32_t				PixelSize;
}FONT_PANEL_PARAMETER;

typedef struct
{
	char					ByteHead[PARAMETER_FORMAT_BYTE_HEAD_LENGTH_MAX+1];
	char					ByteTail[PARAMETER_FORMAT_BYTE_HEAD_LENGTH_MAX+1];
	uint32_t				BytesEachRow;
}FONT_CODE_FORMAT;

typedef struct
{
	FONT_PANEL_PARAMETER	FontPanelParameter;
	FONT_MODULE_MODE		FontModuleMode;
	FONT_BYTE_MODE			FontByteMode;
	FONT_CODE_FORMAT		FontCodeFormat;
}FONT_SETTINGS;

typedef struct
{
	FONT_MODULE_OPTION		FontOption;
	FONT_SETTINGS			Settings;
}APP_RUNTIME_PARAMETER;

#endif // __INCLUDE_PIXELMATRIXFONTDATATYPE_H__

/***************************************************************
 * Name:      PixelMatrixFontApp.h
 * Purpose:   Defines Application Class
 * Author:    Polaris (xuyulin91@163.com)
 * Created:   2017-08-04
 * Copyright: Polaris ()
 * License:
 **************************************************************/

#ifndef _INCLUDE_CLASS_PIXEL_MATRIX_FONT_APP_H__
#define _INCLUDE_CLASS_PIXEL_MATRIX_FONT_APP_H__

#include <wx/app.h>

class PixelMatrixFontApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // _INCLUDE_CLASS_PIXEL_MATRIX_FONT_APP_H__

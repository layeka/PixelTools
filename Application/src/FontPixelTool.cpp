/***************************************************************
 * Name:      PixelMatrixFontApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Polaris (xuyulin91@163.com)
 * Created:   2017-08-04
 * Copyright: Polaris ()
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include <wx/dcclient.h>
#include "FontPixelTool.h"
#include "Frame_Main.h"

IMPLEMENT_APP(PixelMatrixFontApp);

bool PixelMatrixFontApp::OnInit()
{
	Frame_Main*		pclsMainFrame = NULL;

	wxImage::AddHandler(new wxPNGHandler);
	wxImage::AddHandler(new wxJPEGHandler);

    pclsMainFrame = new Frame_Main((wxWindow*)NULL);

    if(NULL != pclsMainFrame)
	{
        // Set main frame icon.
        pclsMainFrame->SetIcon(wxICON(aaaa));
		// Show frame
		pclsMainFrame->Show();
	}

    return true;
}

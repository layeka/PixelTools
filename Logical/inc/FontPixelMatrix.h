#ifndef __INCLUDE_CLASS_PIXELMATRIXFONT_H__
#define __INCLUDE_CLASS_PIXELMATRIXFONT_H__

#include <wx/dcmemory.h>
#include <wx/bitmap.h>
#include <wx/font.h>

class FontPixelMatrix
{
	private:
		wxMemoryDC*					m_pclsMDC;
		wxBitmap*					m_pclsFontBitmap;
		wxFont*						m_pclsFont;

		void						_resizeBitmap(void);
		void						_cutImage(const int32_t iTopCut, const int32_t iBottomCut);
		void						_create(void);
	public:
									FontPixelMatrix(int32_t iPixelSize, wxFontFamily family, wxFontStyle style, wxFontWeight weight, bool underlined = false, const wxString& face = wxEmptyString, wxFontEncoding encoding = wxFONTENCODING_DEFAULT);
									FontPixelMatrix(void);
		void						Create(int32_t iPixelSize, wxFontFamily family, wxFontStyle style, wxFontWeight weight, bool underlined = false, const wxString& face = wxEmptyString, wxFontEncoding encoding = wxFONTENCODING_DEFAULT);

		~FontPixelMatrix(void);
		virtual wxBitmap&			ToBitmap(void);
		virtual	void				SetCharacter(wxUniChar cCode, const int32_t iGraphWidth = -1, const int32_t iTopCut = 0, const int32_t iBottomCut = 0);
		virtual bool				IsOk(void);
};


#endif // __INCLUDE_CLASS_PIXELMATRIXFONT_H__
